<%@ page import="uytube.models.Video" %>
<%@ page import="uytube.models.Lista" %>
<%@ page import="java.util.List"%>
	
<div class="recommended">
	<div class="recommended-grids">
		<div class="recommended-info">
			<h3>Sports <%=request.getParameter("myVar")%></h3>
		</div>
		<% for(Video v:((Lista)l).getVideos()){ %>
		<div class="col-md-3 resent-grid recommended-grid">
			<div class="resent-grid-img recommended-grid-img">
				<a href="single.html"><img src="/web/resources/images/g.jpg"
					alt="" /></a>
				<div class="time small-time">
					<p>7:30</p>
				</div>
				<div class="clck small-clck">
					<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
				</div>
			</div>
			<div class="resent-grid-info recommended-grid-info video-info-grid">
				<h5>
					<a href="single.html" class="title">Varius sit sed viverra
						nullam viverra nullam interdum metus</a>
				</h5>
				<ul>
					<li>
						<p class="author author-info">
							<a href="#" class="author">John Maniya</a>
						</p>
					</li>
					<li class="right-list">
						<p class="views views-info">2,114,200 views</p>
					</li>
				</ul>
			</div>
		</div>
		<%} %>
		<div class="clearfix"></div>
	</div>
</div>
