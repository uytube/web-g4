<div id="myModal" class="modal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crear lista de reproduccion</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="/web/crearLista">
					<div class="form-group">
						<input type="text" class="form-control"
							placeholder="Nombre de la nueva lista" name="nombre_lista"
							id="privado" required="required"
							pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?" />
					</div>
					<div class="form-group">
						<label> <input class="form-control" type="checkbox"
							name="privado" id="privado" />Lista privada?
						</label>
					</div>
					<button type="submit" class="btn btn-success">Crear lista</button>
				</form>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</div>



<style>
.modal-backdrop {
	z-index: 1 !important;
}


.my-mfp-zoom-in.mfp-ready.mfp-bg {
	display: none;
}
</style>