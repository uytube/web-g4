<%@ page import="uytube.models.Usuario"%>
<%@ page import="uytube.models.Canal"%>
<div id="modalEditarUsuario" class="modal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar usuario</h4>
			</div>
			<div class="modal-body">
				<form action="/web/canal" method="post" enctype='multipart/form-data' >
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" value="<%=((Usuario) request.getAttribute("usuario")).getNickname()%>" name="nickname"
									placeholder="Nickname">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" value="<%=((Usuario) request.getAttribute("usuario")).getNombre()%>" name="nombre"
									placeholder="Nombre">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" value="<%=((Usuario) request.getAttribute("usuario")).getApellido()%>" name="apellido"
									placeholder="Apellido">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="email" class="form-control" value="<%=((Usuario) request.getAttribute("usuario")).getCorreo()%>" name="correo"
									placeholder="correo">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
							 <%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>							
								<input type="date" class="form-control" 
								value="<%=df.format(((Usuario) request.getAttribute("usuario")).getFnacimiento())%>" name="fdate"
									placeholder="Fecha de nacimiento">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="password" class="form-control"  name="password"
									placeholder="Password">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" value="<%=((Canal)request.getAttribute("canal")).getNombre()%>" name="canalName"
									placeholder="Nombre del canal">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<textarea class="form-control" name="canalDescription" placeholder="Descripcion del canal">
									<%=((Canal)request.getAttribute("canal")).getDescripcion()%>
								</textarea>
							</div>
						</div>
						<div class="col-md-12 col-md-6">
							<div class="form-group">
								<label> <input type="checkbox" value="<%=((Canal)request.getAttribute("canal")).getPrivacidad()%>" name="canalPrivacity">Es
									privado?
								</label>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
						         <input type = "file" name = "file" size = "50" />
						</div>
						<div class="col-md-12 col-xs-12">
							<button class="btn btn-success" type="submit">Editar usuario</button>
						</div>
					</div>
				</form>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>

		</div>
	</div>
</div>
<style>
.modal-backdrop {
	z-index: 1 !important;
}

.my-mfp-zoom-in.mfp-ready.mfp-bg {
	display: none;
}
</style>