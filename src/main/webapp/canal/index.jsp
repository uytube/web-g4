<%@ page import="uytube.models.Usuario"%>
<%@ page import="uytube.models.Canal"%>
<%@ page import="uytube.models.Lista"%>
<%@ page import="java.util.List"%>
<%@ page import="uytube.models.Video"%>
<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<jsp:include page="../header.jsp"></jsp:include>
<body>
	<jsp:include page="../sideBar.jsp"></jsp:include>
	<section class="col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="row header-canal">
			<div class="col-md-3 col-xs-12">
				<img src="">
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="row" style="display: flex; align-items: center">
					<div class="col-md-3 col-xs-12">
						<h2><%=((Usuario) request.getAttribute("usuario")).getNickname()%></h2>

						<p>
							<%=((Usuario) request.getAttribute("usuario")).getNombre()%>&nbsp;
							<%=((Usuario) request.getAttribute("usuario")).getApellido()%>
							-
							<%=df.format(((Usuario) request.getAttribute("usuario")).getFnacimiento())%>
						</p>
					</div>
					<div class="col-md-9">
						<%
							if ((Usuario) request.getAttribute("usuario") != (Usuario) request.getSession()
									.getAttribute("usuarioLogueado")) {
						%>
						<button class="btn btn-success float-right" data-toggle="modal"
							data-target="#modalEditarUsuario">Editar usuario</button>
						<%
							}
						%>
						<jsp:include page="modalEditarUsuario.jsp"></jsp:include>

					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="recommended">
				<div class="recommended-info">
					<h3>Videos</h3>
				</div>

				<div class="recommended-grids">
					<%
						for (Video v : (List<Video>) request.getAttribute("listaVideos")) {
					%>
					<div class="col-md-3 resent-grid recommended-grid">
						<div class="resent-grid-img recommended-grid-img">
							<%
								String[] idYoutube = v.getUrl().split("/");
							%>
							<a href="ver?id_video=<%=v.getId()%>"><img
								src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
								alt="" /></a>
							<div class="time small-time">
								<p>7:30</p>
							</div>
							<div class="clck small-clck">
								<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
							</div>
						</div>
						<div
							class="resent-grid-info recommended-grid-info video-info-grid">
							<h5>
								<a href="ver?id_video=<%=v.getId()%>" class="title"><%=v.getNombre()%></a>
							</h5>
							<ul>
								<li><a href="/web/modificarVideo?id_video=<%=v.getId()%>"
									class="author">Modificar video</a></li>
							</ul>
						</div>
					</div>
					<%
						}
					%>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<%
						for (Lista l : (List<Lista>) request.getAttribute("listas")) {
					%>
					<div class="recommended">
						<div class="recommended-grids">
							<div class="recommended-info">
								<a href="/web/listasReproduccion?id_lista=<%=l.getId()%>">
									<h3><%=l.getNombre()%></h3>
								</a>
							</div>
							<%
								for (Video v : ((Lista) l).getVideos()) {
							%>
							<div class="col-md-3 resent-grid recommended-grid">
								<div class="resent-grid-img recommended-grid-img">
									<%
										String[] idYoutube = v.getUrl().split("/");
									%>
									<a href="ver?id_video=<%=v.getId()%>"><img
										src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
										alt="" /></a>
									<div class="time small-time">
										<p><%=v.getDuracion()%></p>
									</div>
									<div class="clck small-clck">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div
									class="resent-grid-info recommended-grid-info video-info-grid">
									<h5>
										<a href="ver?id_video=<%=v.getId()%>" class="title"><%=v.getNombre()%></a>
									</h5>
									<ul>
										<li><a
											href="/web/canal?canal=<%=v.getCanal().getUsuario().getNickname()%>"
											class="author"><%=v.getCanal().getNombre()%></a></li>
									</ul>

								</div>
							</div>
							<%
								}
							%>
							<div class="clearfix"></div>
						</div>
						<a href="#">Ver lista</a>
					</div>
					<%
						}
					%>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<ul class="list-group">
						<li class="list-group-item active">Seguidos</li>
						<%
							for (Canal c : (List<Canal>) request.getAttribute("seguidos")) {
						%>
						<li class="list-group-item"><%=c.getNombre()%></li>
						<%
							}
						%>

						<li></li>
					</ul>
				</div>
				<div class="col-md-6 col-xs-12">
					<ul class="list-group">
						<li class="list-group-item active">Seguidores</li>
						<%
							for (Usuario u : (List<Usuario>) request.getAttribute("seguidores")) {
						%>
						<li class="list-group-item"><%=u.getNombre()%></li>
						<%
							}
						%>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/web/resources/js/bootstrap.min.js"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
</body>

</html>
<style>
.header-canal {
	background: #8080805e;
	height: 100px;
	display: flex;
	align-items: center;
}

.float-right {
	float: right;
	margin-right: 5%;
}
</style>