package packageWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uytube.CategoriaController.CategoriaController;
import uytube.CategoriaController.ICategoria;
import uytube.ListaController.ILista;
import uytube.ListaController.ListaController;
import uytube.VideoController.IVideo;
import uytube.VideoController.VideoController;
import uytube.models.Categoria;
import uytube.models.Lista;
import uytube.models.Usuario;
import uytube.models.Video;

/**
 * Servlet implementation class home
 */
public class home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	private ILista controllerLista;
	private CategoriaController controllerCategoria;
	private IVideo controllerVideo;

	public home() {
		super();
		// TODO Auto-generated constructor stub
		controllerLista = new ListaController();
		controllerCategoria = new CategoriaController();
		controllerVideo = new VideoController();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession sessiones = request.getSession();
		System.out.println("PATH:"+request.getRequestURI().substring(request.getContextPath().length()));
		
		/*
		 * Si el usuario esta logeado Cargar las listas
		 */

		// request.setAttribute(name, o) existe unicamente en ese request
		List<Categoria> categorias = controllerCategoria.listarCategorias();
		request.setAttribute("categorias", categorias);
		Categoria categoriaMusica = controllerCategoria.obtenerCategoria("Musica");
		List<Video> musica = controllerVideo.videoPorCategoria(categoriaMusica);

		Categoria categoriaDeporte = controllerCategoria.obtenerCategoria("Deporte");
		List<Video> deporte = controllerVideo.videoPorCategoria(categoriaDeporte);

		Categoria categoriaCiencia = controllerCategoria.obtenerCategoria("Ciencia y tecnologia");
		List<Video> ciencia = controllerVideo.videoPorCategoria(categoriaCiencia);

		Categoria categoriaEntretenimiento = controllerCategoria.obtenerCategoria("Entretenimiento");
		List<Video> entretenimiento = controllerVideo.videoPorCategoria(categoriaEntretenimiento);

		request.setAttribute("videosMusica", musica);
		request.setAttribute("videosDeporte", deporte);
		request.setAttribute("videosCiencia", ciencia);
		request.setAttribute("videosEntretenimiento", entretenimiento);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
