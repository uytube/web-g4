package packageWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uytube.CategoriaController.CategoriaController;
import uytube.ListaController.ListaController;
import uytube.VideoController.VideoController;
import uytube.models.Video;

/**
 * Servlet implementation class misVideos
 */
public class misVideos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VideoController controllerVideo;
	private CategoriaController controllerCategoria;
	private ListaController controllerLista;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public misVideos() {
        super();
        controllerVideo = new VideoController();
        controllerCategoria = new CategoriaController();
        controllerLista = new ListaController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if (request.getSession().getAttribute("usuarioLogueado") != null) {

		String nick = request.getParameter("nick");
		
		List<Video> videos = new ArrayList<Video>();
		videos = controllerVideo.listaVideosUsuario(nick);
		request.setAttribute("videosUsuario", videos);
		 	
		
		request.getRequestDispatcher("misVideos.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
	
	}

}
