package packageWeb;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uytube.CategoriaController.CategoriaController;
import uytube.VideoController.VideoController;
import uytube.models.Categoria;
import uytube.models.Usuario;
import uytube.models.Video;

/**
 * Servlet implementation class modificarVideo
 */
public class modificarVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private VideoController ControllerVideo;
	private CategoriaController controladorCategoria;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public modificarVideo() {
        super();
        // TODO Auto-generated constructor stub
        ControllerVideo = new VideoController();
        controladorCategoria = new CategoriaController();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//obtenemos las categorias
		CategoriaController controladorCategoria = new CategoriaController();
		ArrayList<Categoria> categorias = controladorCategoria.listarCategorias();
		//String[] arrayCategorias = new String[categorias.size()];
		//for(int i = 0; i < arrayCategorias.length; i++) { 
			//arrayCategorias[i] = categorias.get(i).getNombre(); 
		
	
//};
		
		//agregamos las categorias al request
		request.setAttribute("categorias", categorias);
		// obtenemos usuario
		Usuario user = (Usuario)request.getSession().getAttribute("usuarioLogueado");
		//agregamos usuario al request
		request.setAttribute("Usuario", user.getNickname());
		// agregamos video al request
		String id_video = (String) request.getParameter("id_video");
		Video video = ControllerVideo.consultaVideoPorID(Integer.parseInt(id_video));
		//Video video = ControllerVideo.consultaVideo("Show de goles44", usuarioLogueado);
		request.setAttribute("Video", video);
		request.getRequestDispatcher("ModificarVideo.jsp").forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Categoria cat =  controladorCategoria.obtenerCategoria((String)request.getParameter("categoria"));
		String nombr=(String)request.getParameter("nombre");
		String desc=(String)request.getParameter("descripcion");
		String dur=(String)request.getParameter("duracion");
		System.out.println(request.getParameter("id"));
		Video Vid = (Video)ControllerVideo.consultaVideoPorID(Integer.parseInt(request.getParameter("id")));
		Vid.setNombre(nombr);
		Vid.setDescripcion(desc);
		Vid.setDuracion(dur);
		Vid.setFecha(java.sql.Date.valueOf(LocalDate.now()));
		Vid.setCategoria(cat);
		Boolean Privacity;
		if (request.getParameter("privacidad").equals("true")) {
			 Privacity = true;
		} 
		else { Privacity = false;}
		
		Vid.setEs_publico(Privacity);
		Vid.setUrl((String)request.getParameter("url"));
		ControllerVideo.modificarVideo(Vid);
		response.sendRedirect("/web/modificarVideo?id_video=" + Vid.getId());
	}

}
