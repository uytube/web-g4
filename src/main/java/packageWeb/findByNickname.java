package packageWeb;
import uytube.models.Usuario;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import uytube.UsuarioController.IUsuario;
import uytube.UsuarioController.UsuarioController;

/**
 * Servlet implementation class findByUsername
 */
public class findByNickname extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private Gson gson = new Gson();
	private IUsuario controllerUsuario;
    public findByNickname() {
        super();
		controllerUsuario = new UsuarioController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nickname = (String)request.getParameter("nickname");
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Usuario user = controllerUsuario.consultarUsuario(nickname);
		boolean exists;
		if(user!=null) {
			exists = true;
		}else {
			exists = false;
		}
		String userExists = this.gson.toJson(exists);
		out.print(userExists);
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
