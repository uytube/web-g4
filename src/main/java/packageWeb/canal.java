package packageWeb;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uytube.CanalController.CanalController;
import uytube.CanalController.ICanal;
import uytube.CategoriaController.CategoriaController;
import uytube.CategoriaController.ICategoria;
import uytube.ListaController.ILista;
import uytube.ListaController.ListaController;
import uytube.models.Categoria;
import uytube.models.Lista;
import uytube.models.Usuario;
import uytube.models.Video;
import uytube.models.Canal;
import uytube.UsuarioController.IUsuario;
import uytube.UsuarioController.UsuarioController;
import uytube.VideoController.IVideo;
import uytube.VideoController.VideoController;

/**
 * Servlet implementation class canal
 */
public class canal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 100 * 1024;
	private int maxMemSize = 4 * 1024;
	private File file;
	private ILista controllerLista;
	private IUsuario controllerUsuario;
	private ICategoria controllerCategoria;
	private ICanal controllerCanal;
	private IVideo controllerVideo;

	public canal() {
		super();
		controllerLista = new ListaController();
		controllerCategoria = new CategoriaController();
		controllerUsuario = new UsuarioController();
		controllerCanal = new CanalController();
		controllerVideo = new VideoController();
	}

	public void init() {
		// Get the file location where it would be stored.
		filePath = getServletContext().getInitParameter("file-upload");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// categorias
		List<Categoria> categorias = controllerCategoria.listarCategorias();
		request.setAttribute("categorias", categorias);
		// listas
		if (request.getSession().getAttribute("usuarioLogueado") != null) {
			request.setAttribute("nicknameUser",
					((Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname());
		}
		Canal canal = controllerCanal.obtenerCanalUsuario(request.getParameter("canal"));		
		List<Video> videos = controllerVideo.listaVideosUsuario((String) canal.getUsuario().getNickname());
		Usuario user = controllerUsuario.consultarUsuario((String) canal.getUsuario().getNickname());
		request.setAttribute("canal", canal);
		request.setAttribute("usuario", user);
		List<Lista> listas = controllerLista.listarListas((String) canal.getUsuario().getNickname());
		request.setAttribute("listas", listas);
		request.setAttribute("listaVideos", videos);
		List<Canal> seguidos = controllerUsuario.listCanalesSeguidos((String) canal.getUsuario().getNickname());
		List<Usuario> seguidores = controllerUsuario.listUsuariosSeguidores((String) canal.getUsuario().getNickname());
		request.setAttribute("seguidos", seguidos);
		request.setAttribute("seguidores", seguidores);
		request.getRequestDispatcher("canal/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Check that we have a file upload request
		UsuarioController controller = new UsuarioController();
		String canalName = "";
		String canalDescription = "";
		boolean canalPrivacity = false;
		String nickname = "";
		String password = "";
		String nombre = "";
		String apellido = "";
		String correo = "";
		Date fdate = new Date();

		isMultipart = ServletFileUpload.isMultipartContent(request);
		response.setContentType("text/html");
		java.io.PrintWriter out = response.getWriter();

		if (!isMultipart) {
			return;
		}

		DiskFileItemFactory factory = new DiskFileItemFactory();

		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);

		// Location to save data that is larger than maxMemSize.
		// factory.setRepository(new File("c:\\temp"));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);
		String fileName = "";
		try {
			// Parse the request to get file items.
			List<FileItem> fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();
			for (FileItem fi : fileItems) {
				if (!fi.isFormField()) {
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					fileName = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();

					// Write the file
					System.out.println(filePath);
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					fi.write(file);
				} else {
					String fieldname = fi.getFieldName();
					String fieldvalue = fi.getString();
					if (fieldname.equals("canalName")) {
						canalName = fieldvalue;
					} else if (fieldname.equals("canalDescription")) {
						canalDescription = fieldvalue;
					} else if (fieldname.equals("canalPrivacity")) {
						canalPrivacity = (fieldvalue == "true") ? true : false;
					} else if (fieldname.equals("nickname")) {
						nickname = fieldvalue ;
					} else if (fieldname.equals("password")) {
						System.out.println("aca");
						password = fieldvalue;
					} else if (fieldname.equals("nombre")) {
						nombre = fieldvalue;
					} else if (fieldname.equals("apellido")) {
						apellido = fieldvalue;
					} else if (fieldname.equals("correo")) {
						correo = fieldvalue;
					} else if (fieldname.equals("fdate")) {
						try {
							fdate = new SimpleDateFormat("yyyy-MM-dd").parse((String) fieldvalue);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}

		Usuario user = (Usuario) request.getSession().getAttribute("usuarioLogueado");
		System.out.println(user);
		user.setNickname(nickname);
		user.setNombre(nombre);
		user.setCorreo(correo);
		user.setFnacimiento(fdate);
		user.setApellido(apellido);
		user.setImg(filePath + fileName);
		if (password != "") {
		}
		Canal canal = new Canal(canalName, canalDescription, canalPrivacity, user);
		user.addCanal(canal);
		System.out.println(nombre);
		controller.modificarUsuario(user);
		HttpSession sesion = request.getSession();
		sesion.setAttribute("usuarioLogueado", user);
		response.sendRedirect("/wen/canal?nickname=" +user.getNickname());
	}

}
