package packageWeb;
import uytube.UsuarioController.UsuarioController;
import uytube.models.Usuario;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uytube.CategoriaController.CategoriaController;
import uytube.CategoriaController.ICategoria;
import uytube.ListaController.ILista;
import uytube.ListaController.ListaController;
import uytube.models.Categoria;
import uytube.models.Lista;
import uytube.UsuarioController.UsuarioController;
/**
 * Servlet implementation class login
 */
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private ILista controllerListas;
    public login() {
        super();
        // TODO Auto-generated constructor stub
        controllerListas = new ListaController();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Listas de kairoh
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		UsuarioController controller = new UsuarioController();
		String nickname = (String)request.getParameter("user");
		String password = (String)request.getParameter("password");
		System.out.println(nickname+" "+password);
		Usuario user = controller.login(nickname,password);
		List<Lista> listas = controllerListas.listarListas(user.getNickname());		
		System.out.println(user);
		if (user != null) {
			HttpSession sesion = request.getSession();
			sesion.setAttribute("usuarioLogueado", user);
		}
		request.getSession().setAttribute("listas",listas);
		response.sendRedirect("/web/home");
	}

}
