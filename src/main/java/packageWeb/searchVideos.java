package packageWeb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import antlr.debug.MessageAdapter;
import uytube.VideoController.IVideo;
import uytube.VideoController.VideoController;
import uytube.models.Video;

/**
 * Servlet implementation class searchVideos
 */
public class searchVideos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private IVideo videosController;
	private Gson gson;
    public searchVideos() {
        super();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.registerTypeAdapter(Video.class, new MessageAdapter()).create();
        videosController = new VideoController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String search = request.getParameter("q");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		List<Video> videos = videosController.searchVideo(search);
		PrintWriter out = response.getWriter();
		String resultVideos = this.gson.toJson(videos);
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
