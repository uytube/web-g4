package packageWeb;
import uytube.models.Canal;
import uytube.models.Categoria;
import uytube.models.Lista;
import uytube.models.Video;
import uytube.CanalController.CanalController;
import uytube.CanalController.ICanal;
import uytube.CategoriaController.CategoriaController;
import uytube.CategoriaController.ICategoria;
import uytube.ListaController.ILista;
import uytube.ListaController.ListaController;
import uytube.VideoController.IVideo;
import uytube.VideoController.VideoController;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class search
 */
public class search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private ICanal canalController;
	private ILista listaController;
	private IVideo videosController;
	private ICategoria categoriaController;
    public search() {
        super();
        videosController = new VideoController();
        categoriaController = new CategoriaController();
        listaController = new ListaController();
        canalController = new CanalController();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String search = (String)request.getParameter("q");
		String fecha = (String)request.getParameter("fecha");
		String alfabeticamente = (String)request.getParameter("alfabeticamente");
		List<Video> videos = videosController.searchVideo(search, fecha, alfabeticamente);
		List<Canal> canales = canalController.searchCanales(search, alfabeticamente);
		List<Lista> listas = listaController.searchListas(search, alfabeticamente);
		List<Categoria> categorias = categoriaController.listarCategorias();
		request.setAttribute("categorias", categorias);
		request.setAttribute("listaVideos", videos);
		request.setAttribute("listaCanales", canales);
		request.setAttribute("listaListas", listas);
		request.getRequestDispatcher("search.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
