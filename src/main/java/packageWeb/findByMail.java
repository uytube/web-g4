package packageWeb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uytube.models.Usuario;
import com.google.gson.Gson;

import uytube.UsuarioController.IUsuario;
import uytube.UsuarioController.UsuarioController;

/**
 * Servlet implementation class findByMail
 */
public class findByMail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	private Gson gson = new Gson();
	private UsuarioController controllerUsuario;
    public findByMail() {
        super();
		controllerUsuario = new UsuarioController();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String correo = (String)request.getParameter("correo");
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Usuario user = controllerUsuario.consultarEmail(correo);
		System.out.println(user);
		boolean exists;
		if(user!=null) {
			exists = true;
		}else {
			exists = false;
		}
		String userExists = this.gson.toJson(exists);
		out.print(userExists);
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
