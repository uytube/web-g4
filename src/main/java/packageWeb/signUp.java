package packageWeb;

import javax.servlet.annotation.MultipartConfig;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uytube.UsuarioController.UsuarioController;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import uytube.models.Usuario;
import uytube.models.Canal;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Servlet implementation class signup
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 5 * 5)
public class signup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 100 * 1024;
	private int maxMemSize = 4 * 1024;
	private File file;

	public signup() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void init() {
		// Get the file location where it would be stored.
		filePath = getServletContext().getInitParameter("file-upload");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UsuarioController controller = new UsuarioController();
		String canalName = "";
		String canalDescription="";
		boolean canalPrivacity = false;
		String username = "";
		String password = "";
		String nombre = "";
		String apellido = "";
		String correo = "";
		Date fdate = new Date();
		isMultipart = ServletFileUpload.isMultipartContent(request);
		response.setContentType("text/html");
		java.io.PrintWriter out = response.getWriter();

		if (!isMultipart) {
			return;
		}

		DiskFileItemFactory factory = new DiskFileItemFactory();

		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);

		// Location to save data that is larger than maxMemSize.
		// factory.setRepository(new File("c:\\temp"));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);
		String fileName = "";
		try {
			// Parse the request to get file items.
			List<FileItem> fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();
			for (FileItem fi : fileItems) {
				if (!fi.isFormField()) {
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					fileName = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();

					// Write the file
					System.out.println(filePath);
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					fi.write(file);
				} else {
					String fieldname = fi.getFieldName();
					String fieldvalue = fi.getString();
					if (fieldname.equals("canalName")) {
						 canalName = fieldvalue;
					} else if (fieldname.equals("canalDescription")) {
						 canalDescription = fieldvalue;
					} else if (fieldname.equals("canalPrivacity")) {
						 canalPrivacity = (fieldvalue == "true") ? true : false;
					} else if (fieldname.equals("nickname")) {
						 username = fieldvalue + "ahola";
					} else if (fieldname.equals("password")) {
						System.out.println("aca");
						password = fieldvalue;
					} else if (fieldname.equals("nombre")) {
						 nombre = fieldvalue;
					} else if (fieldname.equals("apellido")) {
						 apellido = fieldvalue;
					} else if (fieldname.equals("correo")) {
						 correo = fieldvalue;
					} else if (fieldname.equals("fdate")) {
						try {
							fdate = new SimpleDateFormat("yyyy-MM-dd").parse((String) fieldvalue);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
		Usuario user = new Usuario(username, nombre, apellido, correo, password, fdate, fileName);
		Canal canal = new Canal(canalName, canalDescription, canalPrivacity, user);
		user.addCanal(canal);
		controller.crearUsuario(user, canal);
		HttpSession sesion = request.getSession();
		sesion.setAttribute("usuarioLogueado", user);
		response.sendRedirect("/wen/canal?nickname=" +user.getNickname());

	}

}
